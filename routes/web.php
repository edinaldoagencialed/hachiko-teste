<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('[/]', 'DefaultController@index');

// CRUD Contents
$router->post('/create', 'ContentsController@create');
$router->get('/contents', 'ContentsController@read');
$router->get('/paginate', 'ContentsController@paginate');
$router->put('/contents/{id}[/]', 'ContentsController@update');
$router->delete('/contents/{id}[/]', 'ContentsController@delete');
$router->get('/contents/search[/]', 'ContentsController@search');


// CRUD Usuários
$router->post('/users', 'UsersController@create');
$router->get('/users', 'UsersController@read');
$router->put('/users/{id}[/]', 'UsersController@update');
$router->delete('/users/{id}[/]', 'UsersController@delete');
$router->get('/users/search[/]', 'UsersController@search');


