<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contents extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'type',
        'url'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];
}
