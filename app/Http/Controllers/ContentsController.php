<?php

namespace App\Http\Controllers;

use App\Contents;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContentsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contents();
    }

    public function create(Request $request){
        $form = $request->all();
        $validator = Validator::make($form, $this->model->rules);
        if ($validator->fails()) {
            $res = [
                'status'=>500,
                'data'=>$validator->errors()
            ];
            return response()->json($res,500);
        }else{
            //Tratar a URL
            $form['url']=$this->url_verify($form['title'],$this->model);
            //Inserir no banco de dados
            $entity = $this->model->create($form);
            //Tratar o response
            $res = [
                'status'=>200,
                'data'=>$entity
            ];
            return response()->json($res);
        }
    }

    public function read(){
        $collection = $this->model->all();
        return response()->json($collection);
    }

    public function update(Request $request){

        $entity = $this->model->find($request->route('id'));

        if(isset($entity->id)){
            $put = $entity->update($request->all());
            $res = [
                'status'=>200,
                'data'=>$entity
            ];
            return response()->json($res);
        }else{
            $res = [
                'status'=>500,
                'message'=>'Registro não encontrado!'
            ];
            return response()->json($res,500);
        }
    }

    public function delete(Request $request){

        $entity = $this->model->find($request->route('id'));

        if(isset($entity->id)){
            $delete = $entity->delete();
            $res = [
                'status'=>200,
                'data'=>$delete
            ];
            return response()->json($res);
        }else{
            $res = [
                'status'=>500,
                'message'=>'Registro não encontrado!'
            ];
            return response()->json($res,500);
        }
    }

    // Cria a paginação de conteúdo
    public function paginate(){

        $content = $this->pagination(5, 'asc');
        
        if(isset($content)){
            $res = [
                'status' => 200,
                'data' => $content
            ];

        } else {

            $res = [
                'status' => 500,
                'data' => 'Não há conteudo cadastrado'
            ];

        }

        return response()->json($res);       
    }
}
