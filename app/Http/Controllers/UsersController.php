<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Users();
    }

    public function create(Request $request)
    {
        $form = $request->all();
        $validator = Validator::make($form, $this->model->rules)
        ;
        if ($validator->fails()) {
            $res = [
                'status' => 500,
                'data' => $validator->errors()
            ];
            return response()->json($res, 500);
        } else {

            //Gera uma senha Criptografada
            $password = password_hash($form['password'], PASSWORD_BCRYPT);
            $form['password'] = $password;

            //Inserir no banco de dados
            $entity = $this->model->create($form);
            //Tratar o response
            $res = [
                'status' => 200,
                'data' => $entity
            ];
            return response()->json($res);
        }
    }

    public function read()
    {
        $collection = $this->model->all();
        return response()->json($collection);
    }

    public function update(Request $request)
    {

        $entity = $this->model->find($request->route('id'));

        if (isset($entity->id)) {
            $put = $entity->update($request->all());
            $res = [
                'status' => 200,
                'data' => $entity
            ];
            return response()->json($res);
        } else {
            $res = [
                'status' => 500,
                'message' => 'Registro não encontrado!'
            ];
            return response()->json($res, 500);
        }
    }

    public function delete(Request $request)
    {

        $entity = $this->model->find($request->route('id'));

        if (isset($entity->id)) {
            $delete = $entity->delete();
            $res = [
                'status' => 200,
                'data' => $delete
            ];
            return response()->json($res);
        } else {
            $res = [
                'status' => 500,
                'message' => 'Registro não encontrado!'
            ];
            return response()->json($res, 500);
        }
    }
}
