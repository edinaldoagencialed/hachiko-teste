<?php

namespace App\Http\Controllers;
use App\Brands;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DefaultController extends Controller
{
    protected $model;

    public function __construct()
    {
         
    }

    public function index(Request $request){

            $entity = ['message'=>'Welcome!'];
            return response()->json($entity);
    
    }

    
}
